<?php

namespace App\Controllers;

class Home extends BaseController
{
    
/*************************************************************
* Calculo de la letra del NIF 
* Es una prueba
***************************************************************/ 
    public function index()
    {     
       $frase = "El perro de San Roque";
       $contadores = [
           'A' => 0,
           'E' => 0,
           'I' => 0,
           'O' => 0,
           'U' => 0
       ];
       foreach ($contadores as $key => $contador) {
            //para cada contador o vocal
            for ($i = 0; $i < strlen($frase); $i++) {
                if (strtoupper($frase[$i]) === $key) {
                    $contadores[$key]++;
                }
            }
        }
        echo '<pre>';
        print_r($contadores);
        echo '</pre>';
        //return view('welcome_message');
    }
    
    public function tablaMultiplicar($m = 7){
        for($i=0;$i<=10;$i++){
            echo "$m x $i = ",$m*$i;
            echo "<br>";
        }
    }
/*************************************************************
* Calculo de la letra del NIF 
* Es una prueba
***************************************************************/     
    public function calcularLetra($dni){
       echo "La posición $posicion de $clave corresponde a: ",$this->getLetraNif($dni);
    }
/*************************************************************
* Verificar que de la letra del corresponde con el número NIF 
* Parámetros: $dni: número del dni
*             $letra: letra del dni    
***************************************************************/      
    public function comprobarNif($dni, $letra){
       if ($this->getLetraNif($dni) === $letra){
          echo "Letra $letra corresponde a $dni";
       } else {
          echo "la letra de $dni debería ser: ". $clave[$posicion];
       }
    }
    
    
    public function form(){
        $data['title'] = 'Formulario de Entrada';
        return view('alumnos/form', $data);
        /*
        echo '<form action="http://localhost:8080/codeigniter/index.php/alumnos" method="post">',"\n";
        echo '<label for="texto">Introduce el texto a buscar: </label>',"\n";
        echo '<input type="text" name="texto" value="Pon un valor" id="texto" />',"\n";
        echo '<input type="submit" name="enviar" value="Enviar" />',"\n";
        echo '</form>',"\n";
         * 
         */
    }
    
    public function formBuscaAlumnos(){
        helper('form');
        echo form_open('alumnos');
        echo form_label('Introduce el texto a buscar: ','texto');
        echo form_input('texto', 'Pon un valor', ['id'=>'texto', 'class'=>'bg-danger']);
        echo form_submit('enviar','Enviar');
        echo form_close();
    }
    
   
    
    
    public function muestraAlumnos(){
        $data['title'] = 'Listado de Alumnos';
        $alumnos = new \App\Models\Alumnos();
        $textoabuscar = $this->request->getPost('texto');
        $data['resultado'] = $alumnos->where(['apellido1'=>strtoupper($textoabuscar)])->findAll();
        /*echo "<pre>";
        print_r($resultado);
        echo "</pre>";*/
        return view('alumnos/lista',$data);
    }
    
    private function getLetraNif($dni){
       $clave = 'TRWAGMYFPDXBNJZSQVHLCKE'; //proporcionada por hacienda
       $posicion = $dni % 23; //utilizo el operado resto de división entera (modulo) 
       return $clave[$position]; 
    }
    
    public function alumnosGrupo($grupo){
        $alumnos = new \App\Models\Alumnos();
        $data['resultado'] = $alumnos
                    ->select('alumnos.nombre, alumnos.apellido1, alumnos.apellido2')
                    ->join('matricula','matricula.NIA=alumnos.NIA','LEFT')
                    ->where(['matricula.grupo'=>$grupo])
                    ->findAll();
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
    
    
}
