<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\HotelModel;
use Config\Services;

/**
 * Description of HotelController
 *
 * @author a022590299k
 */
class HotelController extends BaseController{
    
    public function index(){
        
        $hotelModel = new HotelModel();
        // helper('form');
        $data['title'] = 'Listado de Hoteles';
        $data['hoteles'] = $hotelModel->findAll();
        
        /* echo '<pre>';
        print_r($hotelModel->findAll());
        echo '</pre>'; */
        
        return view('hoteles/lista',$data);
    }



    public function inserta(){
        $data['title'] = 'Formulario Alta Hotel';
        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') { 
           return view('hoteles/alta', $data); 
        } else {
            $hotel_nuevo = [
            'nombre' => $this->request->getPost('nombre'),
            'localidad' => $this->request->getPost('localidad'),
            'email' => $this->request->getPost('email'),
            'direccion' => $this->request->getPost('direccion'),
            'descripcion' => $this->request->getPost('descripcion'),
            'cp' => $this->request->getPost('cp'),
        ];
            $hotelModel = new HotelModel();
            if ($hotelModel->insert($hotel_nuevo) === false){
               $data['errores'] = $hotelModel->errors(); 
               return view('hoteles/alta', $data);  
          }
        }
        echo "El hotel ha sido añadido correctamente.";
        return redirect()->to('hoteles/lista'); 
     }
     
     
     public function editar($id){
        
        helper('form');
        $hotelModel = new AlumnoModel();
        $data['hotel'] = $hotelModel->find($id);
        $data['title'] = 'Modificar Hotel';
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
        return view('hoteles/editar',$data);
    }
     
}
        
      
      
      
      
      
      
      
      
      
      
      
      
 
    