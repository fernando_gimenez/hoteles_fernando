<?= $this->extend('templates/default') ?>

<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <ul class="nav justify-content-end mb-4">
        <li class="nav-item ">
            <a class="nav-link active btn btn-primary" href="<?=site_url('hoteles/alta')?>">Añadir Hotel</a>
        </li>
    </ul>

<table class="table table-striped" id="myTable">
        <thead>
            <th>
                Nombre
            </th>
            <th>
                Localidad
            </th>
            <th>
                Direccion
            </th>
            <th>
                CP
            </th>
            <th>
                E-mail
            </th>
            
        </thead>
        <tbody>
        <?php foreach ($hoteles as $hotel): ?>
            <tr>
                <td>
                    <?= $hotel->nombre ?>
                </td>
                <td>
                    <?= $hotel->localidad ?>
                </td>
                <td>
                    <?= $hotel->direccion ?>
                </td>
                <td>
                    <?= $hotel->cp ?>
                </td>
                <td>
                    <?= $hotel->email ?>
                </td>
                
                <td class="text-right">
                        <a href="<?=site_url('hoteles/editar/'.$hotel->id)?>">
                            <span class="bi bi-pencil-square" title="Editar el hotel"></span>
                        </a>
                         <a href="<?=site_url('hoteles/borrar/'.$hotel->id)?>" onclick="return confirm('¿Deseas borrar el hotel <?=$hotel->nombre?>?')">
                            <span class="bi bi-eraser-fill" title="Borrar el hotel"></span>
                        </a>
                </td>
                
            </tr>
        <?php endforeach; ?>
        </tbody>    
    </table>
<?= $this->endSection() ?>
