<?= $this->extend('templates/default') ?>

<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?= form_open('hoteles/formeditar/' . $hotel->id) ?>
    <div class="form-group row">
        <?= form_label('Nombre:', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('nombre', $hotel->nombre, ['class' => 'form_control', 'id' => 'codigo']) ?>
        </div>
    </div>
    <div class="form-group row">
        <?= form_label('Localidad:', 'localidad', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('localidad', $hotel->localidad, ['class' => 'form_control', 'id' => 'localidad']) ?>
        </div>
    </div>
    <div class="form-group row">
        <?= form_label('Email:', 'email', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('email', $hotel->email, ['class' => 'form_control', 'id' => 'email']) ?>
        </div>
    </div>
    <div class="form-group row">
        <?= form_label('Direccion:', 'direccion', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('direccion', $hotel->direccion, ['class' => 'form_control', 'id' => 'direccion']) ?>
        </div>
    </div>
    <div class="form-group row">
        <?= form_label('Descripcion:', 'descripcion', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('descripcion', $hotel->descripcion, ['class' => 'form_control', 'id' => 'descripcion']) ?>
        </div>
    </div>
    <div class="form-group row">
        <?= form_label('CP:', 'cp', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('cp', $hotel->cp, ['class' => 'form_control', 'id' => 'cp']) ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-10">
            <?= form_submit('botoncito', 'Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= form_close() ?>
<?= $this->endSection() ?>