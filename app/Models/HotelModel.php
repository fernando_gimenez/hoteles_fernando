<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of HotelModel
 *
 * @author a022590299k
 */
class HotelModel extends Model{
    //put your code here
    protected $table = 'hoteles';
    protected $primaryKey = 'id';
    protected $returnType = 'object'; 
    protected $allowedFields = ['nombre','localidad','email','direccion','descripcion','cp'];
    protected $validationRules = [
        'nombre'      => 'is_unique[hoteles.nombre]',
        'localidad'   => 'required',
        'email'       => 'valid_email',
        'direccion'   => 'required',
        'descripcion' => 'required',
        'cp'          => 'required','numeric','max_length[5]','min_length[5]',
    ];
}
